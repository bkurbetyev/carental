<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Testing and debugging purposes
Route::get('/test', function() {
//    $userId = DB::table('users')->select(DB::raw('id'))->where('email', 'sadf@asdfasf.com')->first();
//    dd($userId->id);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'ListingController@index')->name('listing.index');
Route::get('/show/{listing}', 'ListingController@show')->name('listing.show');
Route::post('/show/{listing}/review', 'ReviewController@store')->name('review.store');

Route::get('/featured', 'ListingController@featured')->name('listing.featured');
