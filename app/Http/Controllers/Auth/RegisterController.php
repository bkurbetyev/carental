<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    protected $redirectTo = '/home';


    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    {
        // Insert a user into the DB
        DB::insert('
            insert into `users` (first_name, last_name, email, password, active, type)
            values (?, ?, ?, ?, ?, ?)
        ', [
            $request->first_name, $request->last_name, $request->email, bcrypt($request->password), 1, 'user'
        ]);

        // Retrieve a newly inserted user ID
        $userId = DB::table('users')->select(DB::raw('id'))->where('email', $request->email)->first()->id;

        // Login the user
        $this->guard()->loginUsingId($userId);

        // Redirect to home page
        return redirect('/home');
    }

    protected function guard()
    {
        return Auth::guard();
    }
}
