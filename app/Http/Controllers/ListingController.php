<?php

namespace App\Http\Controllers;

use App\Listing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ListingController extends Controller
{
    public function featured()
    {
        // Read from an SQL view into a variable
        $featured = DB::select('select * from featured_listings');

        // Send the variable to blade view
        return view('pages.listings.featured', compact('featured'));
    }

    public function index()
    {
        $listings = Listing::where('active', 1)->paginate(10);
        return view('pages.listings.index', compact('listings'));
    }

    public function show(Listing $listing)
    {
        $listing->views++;
        $listing->save();

        $listing->load(['reviews' => function($query) {
            $query->orderBy('created_at', 'desc');
        }])->get();

        return view('pages.listings.show', compact('listing'));
    }
}
