<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $guarded = [];

    public function listing()
    {
        return $this->hasOne(Listing::class);
    }
}
