<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturedListingsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
            create view featured_listings
            as
            select 
                l.id as id, l.title, l.description, l.price, c.make, c.model, c.color, c.year
                from listings as l
            join cars as c on l.car_id = c.id 
            where l.views >= 80
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('drop view featured_listings');
    }
}
