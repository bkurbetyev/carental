<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('password'),
        'active' => 1,
        'type' => 'user',
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Listing::class, function (Faker\Generator $faker) {
    return [
        'car_id' => 1,
        'title' => $faker->paragraph(1),
        'description' => $faker->paragraph(4),
        'views' => $faker->randomNumber(2),
//        'active' => $faker->boolean(50),
        'active' => 1,
        'price' => $faker->randomFloat(2,50, 500)
    ];
});

$factory->define(App\Review::class, function (Faker\Generator $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 200),
        'title' => $faker->paragraph(1),
        'comment' => $faker->paragraph(2),
        'rating' => $faker->randomFloat(1, 0, 5)
    ];
});

$factory->define(App\Car::class, function (Faker\Generator $faker) {
    return [
        'make' => 'Nissan',
        'model' => 'Skyline R32',
        'year' => '1991',
        'color' => 'grey',
        'transmission' => 'Manual',
        'kilometers' => '100000',
    ];
});