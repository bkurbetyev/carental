<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class)->create([
            'email' => 'badyabmx@gmail.com',
            'password' => bcrypt('password')
        ]);

        factory(\App\User::class, 300)->create()->each( function($u) {
            $u->listings()->save(factory(\App\Listing::class)->make())->each( function($l) {
                $l->reviews()->save(factory(\App\Review::class)->make());
            });
        });
    }
}
