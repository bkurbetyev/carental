@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      @foreach($featured as $listing)
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              {{ $listing->title }}
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12">
                  <img class="img-responsive" src="/img/placeholders/car-placeholder.png" alt="car-placeholder">
                </div>
                <div class="col-md-12">
                  <h3>
                    {{ ucfirst($listing->color) }}
                    <strong>{{ $listing->model }}</strong>
                    (MY{{ $listing->year }})
                  </h3>
                  <p>{{ $listing->description }}</p>
                  <a href="{{ route('listing.show', $listing->id) }}" class="pull-left btn btn-default">View Listing</a>
                  <h2 class="h4 pull-right">AU${{ $listing->price }} / Day</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
@endsection