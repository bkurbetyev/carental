@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            {{ $listing->title }}
            <p class="pull-right">
              Views Count: {{ $listing->views }}
            </p>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-3">
                <img class="img-responsive" src="/img/placeholders/car-placeholder.png" alt="car-placeholder">
              </div>
              <div class="col-md-9">
                <p>
                  {{ ucfirst($listing->car->color) }}
                  <strong>{{ $listing->car->model }}</strong>
                  (MY{{ $listing->car->year }})
                  {{ $listing->car->kilometers }}KMS
                </p>
                <p>{{ $listing->description }}</p>
                <button class="btn btn-primary">BOOK NOW</button>
                <h2 class="h4 pull-right">AU${{ $listing->price }} / Day</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-7 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-body">
            <h3 class="h4">Leave a review</h3>
            <form action="{{ route('review.store', $listing) }}" method="POST">
              {{ csrf_field() }}
              <div class="form-group">
                <input type="text" name="title" class="form-control" placeholder="Title">
              </div>
              <div class="form-group">
                <textarea name="comment" id="" rows="4" class="form-control" placeholder="Leave a review as {{ auth()->user()->fullName }}"></textarea>
              </div>
              <select name="rating" id="" class="form-control">
                <option value="" selected disabled>Select Rating</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </select>
              <br>
              <input type="submit" class="btn btn-primary" value="Save">
            </form>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            Reviews
          </div>
          <div class="panel-body">
            @if(session()->has('success'))
              <div class="alert alert-success">
                {{ session()->get('success') }}
              </div>
            @endif
            <div class="list-group">
              @foreach($listing->reviews as $review)
              <a href="#" class="list-group-item">
                <div class="row">
                  <div class="col-md-2">
                    <img class="img-responsive" src="/img/placeholders/avatar-placeholder.svg" alt="">
                  </div>
                  <div class="col-md-10">
                    <h4 class="list-group-item-heading">{{ $review->title }}</h4>
                    <p class="list-group-item-text">
                      {{ $review->comment }}
                    </p>
                    <p>
                      @for($i=0;$i<$review->rating;$i++)
                        <i class="fa fa-star"></i>
                      @endfor
                    </p>
                  </div>
                </div>
              </a>
              @endforeach
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="panel panel-default">
          <div class="panel-heading">Get in Touch</div>
          <div class="panel-body">
            <form action="" method="POST">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Your Name">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Your Email">
              </div>
              <div class="form-group">
                <textarea name="message" rows="3" class="form-control" placeholder="Your Message.."></textarea>
              </div>
              <button class="btn btn-default btn-block" type="button">SEND</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection