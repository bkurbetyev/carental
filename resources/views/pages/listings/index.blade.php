@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">

        @foreach($listings as $listing)
          <div class="panel panel-default">
            <div class="panel-heading">
              {{ $listing->title }}
              <p class="pull-right">
                Views Count: {{ $listing->views }}
              </p>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3">
                  <img class="img-responsive" src="/img/placeholders/car-placeholder.png" alt="car-placeholder">
                </div>
                <div class="col-md-9">
                  <p>
                    {{ ucfirst($listing->car->color) }}
                    <strong>{{ $listing->car->model }}</strong>
                    (MY{{ $listing->car->year }})
                    {{ $listing->car->kilometers }}KMS
                  </p>
                  <p>{{ $listing->description }}</p>
                  <a href="{{ route('listing.show', $listing) }}" class="pull-left btn btn-default">View Listing</a>
                  <h2 class="h4 pull-right">AU${{ $listing->price }} / Day</h2>
                </div>
              </div>
            </div>
          </div>
        @endforeach

        {{ $listings->links() }}
      </div>
    </div>
  </div>
@endsection